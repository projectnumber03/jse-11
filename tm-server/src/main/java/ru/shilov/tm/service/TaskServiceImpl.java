package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

@AllArgsConstructor
public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    @Getter
    @NotNull
    private final ITaskRepository repository;

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(projectId)) throw new NoSuchEntityException();
        return repository.findByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@Nullable String userId, @Nullable String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        return repository.findByNameOrDescription(userId, value);
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeOneByUserId(userId, id);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value) || Strings.isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return repository.getId(userId, value);
    }

    private boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+");
    }

}
