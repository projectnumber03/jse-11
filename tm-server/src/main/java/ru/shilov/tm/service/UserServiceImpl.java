package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISettingRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.util.SignatureUtil;

@AllArgsConstructor
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @Getter
    @NotNull
    private final IUserRepository repository;

    @NotNull
    private final ISettingService settingService;

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeOneByUserId(userId, id);
    }

    @Override
    public @Nullable User register(@Nullable final User user) {
        if (user == null) throw new EntityPersistException();
        @NotNull final String salt = settingService.findByName("salt").getValue();
        @NotNull final String cycle = settingService.findByName("cycle").getValue();
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        return repository.persist(user);
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws RuntimeException {
        if (user == null) throw new EntityMergeException();
        @NotNull final String salt = settingService.findByName("salt").getValue();
        @NotNull final String cycle = settingService.findByName("cycle").getValue();
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        return super.merge(user);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value)) throw new NumberToIdTransformException(value);
        return repository.getId(value);
    }

    @NotNull
    @Override
    public Boolean containsLogin(@Nullable final String login) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(login)) throw new NoSuchEntityException();
        return repository.containsLogin(login);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws NoSuchEntityException {
        @Nullable final User user;
        if (Strings.isNullOrEmpty(login) || ((user = repository.findByLogin(login)) == null)) throw new NoSuchEntityException();
        return user;
    }

    private boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+");
    }

}
