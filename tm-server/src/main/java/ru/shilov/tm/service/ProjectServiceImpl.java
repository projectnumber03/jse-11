package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.ITaskRepository;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public final class ProjectServiceImpl extends AbstractService<Project> implements IProjectService {

    @Getter
    @NotNull
    private final IProjectRepository repository;

    @NotNull
    private final ITaskRepository taskRepo;

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@Nullable String userId, @Nullable String value) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
        return repository.findByNameOrDescription(userId, value);
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        taskRepo.findByUserId(userId).stream()
                .filter(t -> id.equals(t.getProjectId()))
                .map(Task::getId)
                .collect(Collectors.toList())
                .forEach(taskId -> taskRepo.removeOneByUserId(userId, taskId));
        return repository.removeOneByUserId(userId, id);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException {
        if (Strings.isNullOrEmpty(value) || !isNumber(value) || Strings.isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return repository.getId(userId, value);
    }

    private boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+");
    }

}
