package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static ru.shilov.tm.util.SignatureUtil.sign;

@AllArgsConstructor
public class SessionServiceImpl extends AbstractService<Session> implements ISessionService {

    @Getter
    @NotNull
    private final ISessionRepository repository;

    @NotNull
    private final ISettingService settingService;

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeOneByUserId(userId, id);
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String id, @Nullable final String userId) throws NoSuchEntityException {
        if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return repository.findOneByUserId(userId, id);
    }

    @Override
    public void validateSession(@Nullable final Session userSession, @NotNull final List<User.Role> roles) {
        if (userSession == null || !roles.contains(userSession.getRole())) throw new PermissionException();
        validateSession(userSession);
    }

    @Override
    public void validateSession(@Nullable final Session userSession) {
        if (userSession == null) throw new PermissionException();
        @Nullable final Session session = findOneByUserId(userSession.getId(), userSession.getUserId());
        userSession.setSignature(null);
        if (session == null) throw new NoSuchSessionException();
        @Nullable final String salt = settingService.findByName("salt").getValue();
        @Nullable final Integer cycle = Integer.parseInt(settingService.findByName("cycle").getValue());
        if (session.getSignature() != null && !session.getSignature().equals(sign(userSession, salt, cycle))) throw new IllegalSessionException();
        if (ChronoUnit.SECONDS.between(userSession.getCreationDate(), LocalDateTime.now()) > 90) throw new SessionTimeOutException();
    }

}
