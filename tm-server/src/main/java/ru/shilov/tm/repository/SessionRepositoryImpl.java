package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.entity.Session;

import java.util.List;
import java.util.stream.Collectors;

public final class SessionRepositoryImpl extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId()) && id.equals(entity.getId())).findAny().orElse(null);
    }

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> userId.equals(entry.getValue().getUserId()));
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return entities.entrySet().removeIf(entry -> id.equals(entry.getValue().getId()) && userId.equals(entry.getValue().getUserId()));
    }

}
