package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.entity.User;

import java.util.*;
import java.util.stream.Collectors;

public final class UserRepositoryImpl extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && !entry.getValue().getId().equals(userId));
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.values().stream().filter(u -> login.equals(u.getLogin())).findAny().orElse(null);
    }

    @NotNull
    @Override
    public Boolean containsLogin(@NotNull final String login) {
        return entities.values().stream().anyMatch(u -> login.equals(u.getLogin()));
    }

    @NotNull
    @Override
    public String getId(@NotNull final String value) {
        return entities.size() >= Integer.parseInt(value) ? new ArrayList<>(entities.values()).get(Integer.parseInt(value) - 1).getId() : "";
    }

    @NotNull
    @Override
    public List<User> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getId())).collect(Collectors.toList());
    }

}
