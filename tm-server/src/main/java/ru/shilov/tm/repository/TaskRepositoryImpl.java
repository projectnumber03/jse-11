package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.entity.Task;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId()) && projectId.equals(entity.getProjectId())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) {
        return findByUserId(userId).stream()
                .filter(t -> Objects.requireNonNull(t.getName()).contains(value) || Objects.requireNonNull(t.getDescription()).contains(value))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public String getId(@NotNull final String userId, @NotNull final String value) {
        @NotNull
        final List<Task> entities = findByUserId(userId);
        return entities.size() >= Integer.parseInt(value) ? entities.get(Integer.parseInt(value) - 1).getId() : "";
    }

    @NotNull
    @Override
    public List<Task> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> userId.equals(entry.getValue().getUserId()));
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return entities.entrySet().removeIf(entry -> id.equals(entry.getValue().getId()) && userId.equals(entry.getValue().getUserId()));
    }

}
