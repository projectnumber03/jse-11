package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entities = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<T> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        return entities.values().stream().filter(entity -> id.equals(entity.getId())).findAny().orElse(null);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Nullable
    @Override
    public T persist(@NotNull final T entity) {
        return entities.putIfAbsent(entity.getId(), entity);
    }

    @NotNull
    @Override
    public T merge(@NotNull final T entity) {
        return entities.merge(entity.getId(), entity, (oldEntity, newEntity) -> newEntity);
    }

}
