package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public String getId(@NotNull final String userId, @NotNull final String value) {
        @NotNull
        final List<Project> projects = findByUserId(userId);
        return projects.size() >= Integer.parseInt(value) ? projects.get(Integer.parseInt(value) - 1).getId() : "";
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) {
        return findByUserId(userId).stream()
                .filter(p -> Objects.requireNonNull(p.getName()).contains(value) || Objects.requireNonNull(p.getDescription()).contains(value))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Project> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> userId.equals(entity.getUserId())).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> userId.equals(entry.getValue().getUserId()));
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) {
        return entities.entrySet().removeIf(entry -> id.equals(entry.getValue().getId()) && userId.equals(entry.getValue().getUserId()));
    }

}
