package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> findByNameOrDescription(@NotNull final String userId, @NotNull final String value);

    @NotNull
    String getId(@NotNull final String userId, @NotNull final String value);

    @NotNull
    Boolean removeByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findByUserId(@NotNull final String userId);

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id);

}
