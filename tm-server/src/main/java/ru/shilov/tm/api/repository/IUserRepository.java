package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    String getId(@NotNull final String value);

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    User findByLogin(@NotNull final String login);

    @NotNull
    Boolean containsLogin(@NotNull final String login);

    @NotNull
    List<User> findByUserId(@NotNull final String userId);

}
