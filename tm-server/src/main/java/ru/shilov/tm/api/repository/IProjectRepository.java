package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    String getId(@NotNull final String userId, @NotNull final String value);

    @NotNull
    List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value);

    @NotNull
    Boolean removeByUserId(@NotNull final String userId);

    @NotNull
    List<Project> findByUserId(@NotNull final String userId);

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id);

}
