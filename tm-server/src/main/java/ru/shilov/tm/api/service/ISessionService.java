package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session findOneByUserId(@Nullable final String id, @Nullable final String userId) throws NoSuchEntityException;

    @NotNull
    Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException;

    void validateSession(@Nullable final Session userSession);

    void validateSession(@Nullable final Session userSession, @NotNull final List<User.Role> roles);

}
