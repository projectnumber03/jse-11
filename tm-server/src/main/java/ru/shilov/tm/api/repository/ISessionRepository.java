package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findOneByUserId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Boolean removeByUserId(@NotNull final String userId);

    @NotNull
    List<Session> findByUserId(@NotNull final String userId);

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id);

}
