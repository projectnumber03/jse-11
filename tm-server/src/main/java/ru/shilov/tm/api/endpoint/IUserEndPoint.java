package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public interface IUserEndPoint {

    @NotNull
    @WebMethod
    List<User> findAllUsers(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    User findOneUser(@Nullable final Session session, @NotNull final String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final Session session) throws Exception;

    @Nullable
    @WebMethod
    User persistUser(@Nullable final Session session, @Nullable final User u) throws Exception;

    @Nullable
    @WebMethod
    User mergeUser(@Nullable final Session session, @NotNull final User u) throws Exception;

    @NotNull
    @WebMethod
    Boolean removeOneUserByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @NotNull
    @WebMethod
    String getUserId(@Nullable final Session session, @NotNull final String value) throws Exception;

    @Nullable
    @WebMethod
    User registerUser(@Nullable final User u) throws Exception;

}
