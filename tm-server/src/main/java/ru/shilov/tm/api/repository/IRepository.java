package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    T findOne(@NotNull final String id);

    void removeAll();

    @Nullable
    T persist(@NotNull final T entity);

    @NotNull
    T merge(@NotNull final T entity);

}
