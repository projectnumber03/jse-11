package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndPoint {

    @NotNull
    @WebMethod
    List<Task> findAllTasks(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Task findOneTask(@Nullable final Session session, @NotNull final String id) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByUserId(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByProjectId(@Nullable final Session session, @NotNull final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Boolean removeTasksByUserId(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Boolean removeOneTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Task persistTask(@Nullable final Session session, @Nullable final Task t) throws Exception;

    @Nullable
    @WebMethod
    Task mergeTask(@Nullable final Session session, @NotNull final Task t) throws Exception;

    @NotNull
    @WebMethod
    String getTaskId(@Nullable final Session session, @NotNull final String value) throws Exception;

    @NotNull
    List<Task> findTasksByNameOrDescription(@Nullable final Session session, @NotNull final String value) throws Exception;

}
