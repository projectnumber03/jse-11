package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.error.EntityPersistException;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface IDataTransportService {

    void saveDataBin() throws IOException;

    void loadDataBin() throws Exception;

    void saveDataXmlByJaxB() throws JAXBException;

    void loadDataXmlByJaxB() throws Exception;

    void saveDataJsonByJaxB() throws Exception;

    void loadDataJsonByJaxB() throws Exception;

    void saveDataXmlByFasterXml() throws IOException;

    void loadDataXmlByFasterXml() throws Exception;

    void saveDataJsonByFasterXml() throws Exception;

    void loadDataJsonByFasterXml() throws Exception;

}
