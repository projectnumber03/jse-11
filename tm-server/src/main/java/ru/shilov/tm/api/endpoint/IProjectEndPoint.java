package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndPoint {

    @NotNull
    @WebMethod
    List<Project> findAllProjects(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Project findOneProject(@Nullable final Session session, @NotNull final String id) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findProjectsByUserId(@Nullable final Session session) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Boolean removeProjectsByUserId(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    Boolean removeOneProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Project persistProject(@Nullable final Session session, @Nullable final Project p) throws Exception;

    @Nullable
    @WebMethod
    Project mergeProject(@Nullable final Session session, @NotNull final Project p) throws Exception;

    @NotNull
    @WebMethod
    String getProjectId(@Nullable final Session session, @NotNull final String value) throws Exception;

    @NotNull
    List<Project> findProjectsByNameOrDescription(@Nullable final Session session, @NotNull final String value) throws Exception;

}
