package ru.shilov.tm.error;

public class IllegalSessionException extends RuntimeException {

    public IllegalSessionException() {
        super("Сессия недействительна");
    }

}
