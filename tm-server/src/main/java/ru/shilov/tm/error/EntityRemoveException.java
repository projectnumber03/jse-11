package ru.shilov.tm.error;

public final class EntityRemoveException extends RuntimeException {

    public EntityRemoveException() {
        super("Ошибка удаления объекта");
    }

}
