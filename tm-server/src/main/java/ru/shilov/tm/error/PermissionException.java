package ru.shilov.tm.error;

public final class PermissionException extends RuntimeException {

    public PermissionException() {
        super("Недостаточно привилегий");
    }

}
