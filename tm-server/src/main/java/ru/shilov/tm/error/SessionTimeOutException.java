package ru.shilov.tm.error;

public class SessionTimeOutException extends RuntimeException {

    public SessionTimeOutException() {
        super("Сессия устарела, войдите в систему повторно");
    }
}
