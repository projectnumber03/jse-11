package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IUserEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IUserEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class UserEndPointImpl implements IUserEndPoint {

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    public List<User> findAllUsers(@Nullable final Session session) {
        sessionService.validateSession(session);
        return userService.findAll();
    }

    @NotNull
    @Override
    public User findOneUser(@Nullable final Session session, @NotNull final String id) throws Exception {
        sessionService.validateSession(session);
        return userService.findOne(id);
    }

    @Override
    public void removeAllUsers(@Nullable final Session session) {
        sessionService.validateSession(session);
        userService.removeAll();
    }

    @Nullable
    @Override
    public User persistUser(@Nullable final Session session, @Nullable final User u) throws Exception {
        sessionService.validateSession(session);
        return userService.persist(u);
    }

    @Nullable
    @Override
    public User mergeUser(@Nullable final Session session, @NotNull final User u) throws Exception {
        sessionService.validateSession(session);
        return userService.merge(u);
    }

    @NotNull
    @Override
    public Boolean removeOneUserByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        return userService.removeOneByUserId(session.getUserId(), id);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final Session session, @NotNull final String value) {
        sessionService.validateSession(session);
        return userService.getId(value);
    }

    @Nullable
    @Override
    public User registerUser(@Nullable final User u) {
        return userService.register(u);
    }

}
