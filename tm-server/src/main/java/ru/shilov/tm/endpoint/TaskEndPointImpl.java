package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ITaskEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ITaskEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class TaskEndPointImpl implements ITaskEndPoint {

    @NotNull
    private ITaskService taskService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    public List<Task> findAllTasks(@Nullable final Session session) {
        sessionService.validateSession(session);
        return taskService.findAll();
    }

    @NotNull
    @Override
    public Task findOneTask(@Nullable final Session session, @NotNull final String id) throws Exception {
        sessionService.validateSession(session);
        return taskService.findOne(id);
    }

    @NotNull
    @Override
    public List<Task> findTasksByUserId(@Nullable final Session session) {
        sessionService.validateSession(session);
        return taskService.findByUserId(session.getUserId());
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@Nullable final Session session, @NotNull final String id) {
        sessionService.validateSession(session);
        return taskService.findByProjectId(session.getUserId(), id);
    }

    @Override
    public void removeAllTasks(@Nullable final Session session) {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        taskService.removeAll();
    }

    @NotNull
    @Override
    public Boolean removeTasksByUserId(@Nullable final Session session) {
        sessionService.validateSession(session);
        return taskService.removeByUserId(session.getUserId());
    }

    @NotNull
    @Override
    public  Boolean removeOneTaskByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        sessionService.validateSession(session);
        return taskService.removeOneByUserId(session.getUserId(), id);
    }

    @Nullable
    @Override
    public Task persistTask(@Nullable final Session session, @Nullable final Task t) throws Exception {
        sessionService.validateSession(session);
        return taskService.persist(t);
    }

    @Nullable
    @Override
    public Task mergeTask(@Nullable final Session session, @NotNull final Task t) throws Exception {
        sessionService.validateSession(session);
        return taskService.merge(t);
    }

    @NotNull
    @Override
    public String getTaskId(@Nullable final Session session, @NotNull final String value) {
        sessionService.validateSession(session);
        return taskService.getId(session.getUserId(), value);
    }

    @NotNull
    @Override
    public List<Task> findTasksByNameOrDescription(@Nullable final Session session, @NotNull final String value) {
        sessionService.validateSession(session);
        return taskService.findByNameOrDescription(session.getUserId(), value);
    }

}
