package ru.shilov.tm.context;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.api.repository.*;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.endpoint.*;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Setting;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.enumerated.Status;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.repository.*;
import ru.shilov.tm.service.*;
import ru.shilov.tm.util.SignatureUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.time.LocalDate;
import java.util.Scanner;

public final class Bootstrap {

    @NotNull
    private final IProjectRepository projectRepo = new ProjectRepositoryImpl();

    @NotNull
    private final ITaskRepository taskRepo = new TaskRepositoryImpl();

    @NotNull
    private final IUserRepository userRepo = new UserRepositoryImpl();

    @NotNull
    private final ISessionRepository sessionRepo = new SessionRepositoryImpl();

    @NotNull
    private final ISettingRepository settingRepo = new SettingRepositoryImpl();

    @NotNull
    private final IProjectService projectService = new ProjectServiceImpl(projectRepo, taskRepo);

    @NotNull
    private final ITaskService taskService = new TaskServiceImpl(taskRepo);

    @NotNull
    private final ISettingService settingService = new SettingServiceImpl(settingRepo);

    @NotNull
    private final IUserService userService = new UserServiceImpl(userRepo, settingService);

    @NotNull
    private final ISessionService sessionService = new SessionServiceImpl(sessionRepo, settingService);

    @NotNull
    private final IDataTransportService dataTransportService = new DataTrasportServiceImpl(projectService, taskService, userService);

    @NotNull
    private final IProjectEndPoint projectEndpoint = new ProjectEndPointImpl(projectService, sessionService);

    @NotNull
    private final ITaskEndPoint taskEndpoint = new TaskEndPointImpl(taskService, sessionService);

    @NotNull
    private final IUserEndPoint userEndpoint = new UserEndPointImpl(userService, sessionService);

    @NotNull
    private final ISessionEndPoint sessionEndpoint = new SessionEndPointImpl(sessionService, userService, settingService);

    @NotNull
    private final IDataTransportEndPoint dataTransportEndPoint = new DataTransportEndPointImpl(dataTransportService, sessionService);

    public void init() throws Exception {
        System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        new File("./saved").mkdirs();
        initEntities();
        @NotNull final String hostName = settingService.findByName("hostname").getValue();
        @NotNull final String port = settingService.findByName("port").getValue();
        Endpoint.publish(String.format("http://%s:%s/projectservice?wsdl", hostName, port), projectEndpoint);
        Endpoint.publish(String.format("http://%s:%s/taskservice?wsdl", hostName, port), taskEndpoint);
        Endpoint.publish(String.format("http://%s:%s/userservice?wsdl", hostName, port), userEndpoint);
        Endpoint.publish(String.format("http://%s:%s/sessionservice?wsdl", hostName, port), sessionEndpoint);
        Endpoint.publish(String.format("http://%s:%s/datatransportservice?wsdl", hostName, port), dataTransportEndPoint);
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!scanner.nextLine().equals("exit")) { }
        System.exit(0);
    }

    private void initEntities() throws Exception {
        try {
            @NotNull final String salt = settingService.findByName("salt").getValue();
            @NotNull final String cycle = settingService.findByName("cycle").getValue();
            userService.persist(new User("user", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.USER));
            userService.persist(new User("admin", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

}
