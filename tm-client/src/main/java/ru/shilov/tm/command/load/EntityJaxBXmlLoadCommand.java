package ru.shilov.tm.command.load;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityJaxBXmlLoadCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        getEndPointLocator().getDataTransportEndPoint().loadDataXmlByJaxB(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "load-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузка предметной области из xml с использованием JAXB";
    }

}
