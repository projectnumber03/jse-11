package ru.shilov.tm.command.user;

import com.google.common.base.Strings;
import ru.shilov.tm.api.endpoint.Role;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Arrays;

public final class UserMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userId = getEndPointLocator().getUserEndPoint().getUserId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final User u = getEndPointLocator().getUserEndPoint().findOneUser(session, userId);
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        Arrays.asList(Role.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.value())));
        System.out.println("ВЫБЕРИТЕ РОЛЬ:");
        @NotNull String roleId = getServiceLocator().getTerminalService().nextLine();
        while (!roleCheck(roleId)) {
            System.out.println("ВЫБЕРИТЕ РОЛЬ:");
            roleId = getServiceLocator().getTerminalService().nextLine();
        }
        u.setRole(Role.values()[Integer.parseInt(roleId) - 1]);
        getEndPointLocator().getUserEndPoint().mergeUser(session, u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование пользователя";
    }

    @NotNull
    private Boolean roleCheck(@Nullable final String roleId) {
        return !Strings.isNullOrEmpty(roleId)
                && roleId.matches("\\d+")
                && Integer.parseInt(roleId) <= Role.values().length;
    }

}
