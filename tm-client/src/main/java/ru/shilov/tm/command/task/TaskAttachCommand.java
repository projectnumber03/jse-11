package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Task t = getEndPointLocator().getTaskEndPoint().findOneTask(session, taskId);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(session, getServiceLocator().getTerminalService().nextLine());
        t.setProjectId(projectId);
        getEndPointLocator().getTaskEndPoint().mergeTask(session, t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
