package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class TaskSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Task t = getEndPointLocator().getTaskEndPoint().findOneTask(session, taskId);
        getServiceLocator().getTerminalService().printTaskProperties(t);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства задачи";
    }

}
