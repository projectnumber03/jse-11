package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class UserFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<User> users = getEndPointLocator().getUserEndPoint().findAllUsers(getSession());
        getServiceLocator().getTerminalService().printAllUsers(users);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список пользователей";
    }

}
