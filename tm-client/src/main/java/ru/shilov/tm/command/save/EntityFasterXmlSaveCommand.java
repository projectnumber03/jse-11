package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityFasterXmlSaveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        getEndPointLocator().getDataTransportEndPoint().saveDataXmlByFasterXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-xml-f";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в xml с использованием FasterXml";
    }

}
