package ru.shilov.tm.command.task;

import com.google.common.base.Strings;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Status;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

public final class TaskMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Task t = getEndPointLocator().getTaskEndPoint().findOneTask(session, taskId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ЗАДАЧИ:");
        t.setName(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        t.setDescription(getServiceLocator().getTerminalService().nextLine());
        try {
            @NotNull final DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            @NotNull final DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            t.setStart(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            t.setFinish(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        Arrays.asList(Status.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.value())));
        System.out.println("ВЫБЕРИТЕ СТАТУС:");
        @NotNull
        String statusId = getServiceLocator().getTerminalService().nextLine();
        while (!statusCheck(statusId)) {
            System.out.println("ВЫБЕРИТЕ СТАТУС:");
            statusId = getServiceLocator().getTerminalService().nextLine();
        }
        t.setStatus(Status.values()[Integer.parseInt(statusId) - 1]);
        getEndPointLocator().getTaskEndPoint().mergeTask(session, t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование задачи";
    }

    @NotNull
    private Boolean statusCheck(@Nullable final String statusId) {
        return !Strings.isNullOrEmpty(statusId)
                && statusId.matches("\\d+")
                && Integer.parseInt(statusId) <= Status.values().length;
    }

}
