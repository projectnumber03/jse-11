package ru.shilov.tm.command.user;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class UserSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userId = getEndPointLocator().getUserEndPoint().getUserId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final User u = getEndPointLocator().getUserEndPoint().findOneUser(session, userId);
        getServiceLocator().getTerminalService().printUserProperties(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства пользователя";
    }

}
