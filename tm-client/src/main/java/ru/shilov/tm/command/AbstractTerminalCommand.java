package ru.shilov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.context.IEndPointLocator;
import ru.shilov.tm.api.context.IServiceLocator;
import ru.shilov.tm.api.context.ISessionLocator;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.error.NoSuchSessionException;

import java.util.Objects;

public abstract class AbstractTerminalCommand {

    @Setter
    @Nullable
    private IEndPointLocator endPointLocator;

    @Setter
    @Nullable
    private ISessionLocator sessionLocator;

    @Setter
    @Nullable
    private IServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public IEndPointLocator getEndPointLocator() {
        return Objects.requireNonNull(endPointLocator);
    }

    @NotNull
    public ISessionLocator getSessionLocator() {
        return Objects.requireNonNull(sessionLocator);
    }

    @NotNull
    public IServiceLocator getServiceLocator(){
        return Objects.requireNonNull(serviceLocator);
    }

    @NotNull
    public Session getSession() {
        if (getSessionLocator().getSession() == null) throw new NoSuchSessionException();
        @Nullable final Session session = getSessionLocator().getSession();
        if (session == null) throw new NoSuchSessionException();
        return session;
    }

}
