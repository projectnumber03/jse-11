package ru.shilov.tm.command.save;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class EntityJaxBXmlSaveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        getEndPointLocator().getDataTransportEndPoint().saveDataXmlByJaxB(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "save-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранение предметной области в xml с использованием JAXB";
    }

}
