package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class UserPasswordChangeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        @NotNull final User u = getEndPointLocator().getUserEndPoint().findOneUser(session, session.getUserId());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        getEndPointLocator().getUserEndPoint().mergeUser(session, u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "password-change";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля";
    }

}
