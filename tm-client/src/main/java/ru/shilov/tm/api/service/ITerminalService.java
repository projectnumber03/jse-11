package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.AbstractEntity;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.api.endpoint.User;

import java.util.List;

public interface ITerminalService {

    String nextLine();

    void printAllProjects(@NotNull final List<Project> projects);

    void printAllTasks(@NotNull final List<Task> tasks);

    void printAllUsers(@NotNull final List<User> users);

    void printProjectProperties(@NotNull final Project p);

    void printTaskProperties(@NotNull final Task t);

    void printUserProperties(@NotNull final User u);

}
