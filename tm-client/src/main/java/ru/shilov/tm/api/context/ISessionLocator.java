package ru.shilov.tm.api.context;

import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Session;

public interface ISessionLocator {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);

}
